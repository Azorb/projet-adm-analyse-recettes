FROM jupyter/base-notebook:67b8fb91f950
LABEL maintainer="Guillaume Bernard <guillaume.bernard@univ-lr.fr>"

# Les actions de ce Dockerfile doivent être réalisées en tant que root
USER 0

# Définition du répertoire de travail par défaut
WORKDIR /app

# Ajout du fichier de dépendances
ADD requirements.txt /app/requirements.txt

# Installation des dépendances avec Conda dans le Jupyter Notebook
RUN conda install -c anaconda curl && \ 
    conda install --yes --file /app/requirements.txt && \
    conda clean --yes --all

# Téléchargement des ressources aditionnelles pour spacy et nltk
RUN python -m spacy download en_core_web_sm && \
    python -m nltk.downloader punkt stopwords wordnet averaged_perceptron_tagger maxent_ne_chunker words gutenberg reuters

# Transfert de propriété à l’utilisateur utilisé pour lancer le conteneur
RUN chown -R 1000:1000 /app

# Définition de l’utilisateur du conteneur
USER 1000
