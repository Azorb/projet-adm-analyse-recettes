# Analyse de grandes quantités de textes

Fait partie du cours ADM (**A**nalyse de **D**onnées **M**assives) de Semestre 4 en DUT Informatique de La Rochelle. Les séances de travaux dirigées et travaux pratiques sont fusionnées. Vous serez systématiquement en « salle machine » sur un seul et unique sujet de travaux pratiques.

Le fonctionnement de TD/TP repose sur un [**carnet de notes Jupyter**](https://jupyter.org/) qui est une application web qui permet d’écrire à la fois du texte et des formules et d’exécuter du code Python, dans le même document. Nous vous proposons deux méthodes d’installation différentes. **L’utilisation de Docker est recommandée**.

**CHOISISSEZ L’UNE OU L’AUTRE DES SOLUTIONS, PAS LES DEUX**.

## Récupération du projet

Vous allez récupérer les fichiers nécessaires au travail à l’aide de Git, tapez la commande :
```bash
https://gitlab.univ-lr.fr/enseignements-iut-la-rochelle/analyse-de-donnees-massives-adm4-partie-i
```

Vous avez accès à un ensemble de fichiers :
* `requirements.txt` contient l’ensemble des dépendances Python de votre projet
* `docker-compose.yml` la description de votre instance de Jupyter basée sur Docker
* `ADM − Traitement de données textuelles.ipynb` le document sur lequel vous allez travailler : le fichier Jupyter

## Utilisation dans un conteneur Docker (méthode recommandée)

Vous vous apprêtez à partager des ressources locales (sur votre poste) avec le conteneur. Le conteneur est une « machine » autonome avec ses propres utilisateurs. Vous devez donc vous assurer que les « autres » (*o* pour ceux qui ont peu de mémoire…) aient les droits d’écriture (*w*), de lecture (*r*) et d’éxécution sur les répertoires (*x*). La commande à utiliser est `chmod` :

```bash
chmod o+rx .
chmod o+r data/*
chmod o+r img/*
chmod o+r *
chmod o+x data img
```

Nous mettons à votre disposition un conteneur Docker, [disponible sur le site hub.docker.com](https://hub.docker.com/r/guilieb/iutlr-adm4/tags) (`guilieb/iutlr-adm4:partie1`, 1,8Go locaux, 667,9Mo à télécharger). L’utilisation de cette image est simplifiée par l’outil `docker-compose` que vous pouvez utiliser. Commencez par télécharger l’image : `docker-compose pull`. Le conteneur contient l’ensemble des outils (`jupyter` et autres dépendances) qui sont utiles au projet. Ne lancer qu’une seule instance à la fois, puisque que le conteneur occupe le port local 8888. Consultez le fichier `docker-compose.yml` pour plus d’informations.

Dans tous les cas, vous pouvez accéder à l’instance via l’URL : [`http://127.0.0.1:8888`](http://127.0.0.1:8888). Pour lancer le conteneur, utilisez la commande suivante : `docker-compose run --rm --service-ports jupyter` qui vous donnera la sortie suivante : 
``

```
Executing the command: jupyter notebook
[I 13:31:25.359 NotebookApp] Writing notebook server cookie secret to /home/jovyan/.local/share/jupyter/runtime/notebook_cookie_secret
[I 13:31:25.857 NotebookApp] JupyterLab extension loaded from /opt/conda/lib/python3.7/site-packages/jupyterlab
[I 13:31:25.858 NotebookApp] JupyterLab application directory is /opt/conda/share/jupyter/lab
[I 13:31:26.118 NotebookApp] Serving notebooks from local directory: /app
[I 13:31:26.118 NotebookApp] The Jupyter Notebook is running at:
[I 13:31:26.119 NotebookApp] http://9fc80509e3c0:8888/?token=39fe43c1bd348015bc0398e0ef49296e5acc699abd3fdb31
[I 13:31:26.119 NotebookApp]  or http://127.0.0.1:8888/?token=39fe43c1bd348015bc0398e0ef49296e5acc699abd3fdb31
[I 13:31:26.119 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
[C 13:31:26.124 NotebookApp] 
    
    To access the notebook, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-6-open.html
    Or copy and paste one of these URLs:
        http://9fc80509e3c0:8888/?token=39fe43c1bd348015bc0398e0ef49296e5acc699abd3fdb31
     or http://127.0.0.1:8888/?token=39fe43c1bd348015bc0398e0ef49296e5acc699abd3fdb31
```

Ouvrez donc le lien qui vous est proposé, et vous accéder à `jupyter`. Ouvrez le fichier `.ipynb` pour commencer le TP.

### En cas de problème…

Si jamais vous rencontrez des problèmes de réseau quant au téléchargement de l’image Docker, vous pouvez l’importer manuellement. Elle est disponible sur le [cours Moodle : **DUT-INFO-S4-ADM4 - Analyse de donnees massives**](https://moodle.univ-lr.fr/2020/enrol/index.php?id=6904). Vous devez la télécharger puis lancer la commande `docker load --input /chemin/vers/fichier.tar.gz`. Plus d’information et d’explication sur la documentation dédiée à la commande [`docker load`](https://docs.docker.com/engine/reference/commandline/load/).

## Installation dans un environnement Python virtuel

Vérifiez que vous **disposez d’une version de Python 3 (j’insiste sur le 3 !)** fonctionnelle. Si vous ne savez pas, [veuillez télécharger la dernière version disponible depuis python.org](https://www.python.org/downloads/). Après installation, vous devriez pouvoir taper la commande `python3` sous GNU/Linux ou MacOS X, et `C:\Python38\python.exe` sur Windows. À chaque fois que vous voyez une commande `python3`, merci de remplacer par la commande correspondante pour votre système.

Nous allons procéder par étapes…

1. **Créez un environnement virtuel** : **indépendant de votre environnment Python système**, il vous permettra d’installer des dépendances sans modifier le reste du système. Attention cependant, certaines dépendances doivent être compilées, aussi assurez vous d’avoir un environnement de compilation complet pour le C et C++. On créé cet environnement en se déplaçant dans le répertoire de travail, par exemple : `cd ~/Documents/Projets/ADM-AnalyseDeTextes` et en exécutant la commande `python3 -m venv venv`. Cela crée un répertoire nommé `venv` qui contient toutes les références à votre nouvel environnement Python.
2. **Activez votre environnement virtuel**, avec, sous GNU/Liunx, la commande `source venv/bin/activate`. Sous Windows, la commande est `.\venv\Scripts\Activate.ps1`. Si PowerShell vous renvoie balader, [suivez ces instructions](https://www.tenforums.com/tutorials/54585-change-powershell-script-execution-policy-windows-10-a.html).  Vous voyez que votre « *prompt* » change et qu’au début est affiché le nom de votre répertoire. Vous êtes dans votre environnement virtuel. 
3. **Vérifiez votre environnement virtuel** : vérifiez l’activation de l’environnement virtuel en tapant la commande `which python3` qui doit, sous Linux et MacOS X vous indiquer que l’exécutable Python est localisé dans votre répertoire `venv`. Sous Windows, la commande est `Get-Command python`.
4. Vous pouvez désormais **installer vos dépendances** en utilisant la commande `pip`, par exemple : `pip install nltk` pour installer l’**outil de traitement du langage naturel**. Pour installer les dépendances du projet, exécutez `pip install -r requirements.txt`.
5. **Démarrez Jupyter Notebook**, avec la commande `jupyter notebook`. Accédez au lien qui vous est indiqué par la sortie du terminal.

Pour ceux qui doutent, voici une [référence OpenClassroom](https://openclassrooms.com/fr/courses/4425111-perfectionnez-vous-en-python/4463278-travaillez-dans-un-environnement-virtuel).
